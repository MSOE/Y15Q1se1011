package wk8;

import java.util.Random;
import java.util.Scanner;

public class FractionGame {
    public static void main(String[] args) {
        Random rand = new Random();
        int num1 = 1; //rand.nextInt(21) - 10;
        int den1 = 2; //rand.nextInt(10) + 1;
        int num2 = 2; //rand.nextInt(21) - 10;
        int den2 = 1; //rand.nextInt(10) + 1;
        System.out.println(num1 + "/" + den1 + " + " + num2 + "/" + den2 + " = ?");
        Scanner in = new Scanner(System.in);
        String answer = in.next();
        int slashIndex = answer.indexOf('/');
        int numUser;
        int denUser;
        if(slashIndex==-1) {
            numUser = Integer.parseInt(answer);
            denUser = 1;
        } else {
            numUser = Integer.parseInt(answer.substring(0, slashIndex));
            denUser = Integer.parseInt(answer.substring(slashIndex + 1));
        }
        int numAns = num1*den2 + num2*den1;
        int denAns = den1*den2;
        String display;
        display = generateDisplayText(numUser, denUser, numAns, denAns);
        System.out.println(display);

    }

    private static String generateDisplayText(int numUser, int denUser, int numAns, int denAns) {
        String display;
        double user = (double)numUser/denUser;
        double ans = (double)numAns/denAns;
        if(user==ans) {
            display = "You are very smart.";
        } else {
            // CONDITIONAL ? TRUE RESULT : FALSE RESULT
            display = "The correct answer is: " + (denAns==1 ? numAns : numAns + "/" + denAns);
        }
        return display;
    }
}
