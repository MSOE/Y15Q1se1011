package wk7;

/*
 +--------------------------------------+
 |            Fraction                  |
 +--------------------------------------+
 | - num: int                           |
 | - den: int                           |
 +--------------------------------------+
 | + Fraction()                         |
 | + Fraction(num: int)                 |
 | + Fraction(num: int, den: int)       |
 | + Fraction(fraction: String)         |
 | + toString(): String                 |
 | - reduce(): void                     |
 | + plus(that: Fraction): Fraction     |
 | + equals(that: Fraction): boolean    |
 +--------------------------------------+
 */
public class Fraction {
    public static void main(String[] args) {
        Fraction frac = new Fraction();
        if(frac.num>10 || frac.num<-10) {
            System.err.println("Numerator outside of range");
        }
        if(frac.den>10 || frac.den<1) {
            System.err.println("Denominator outside of range");
        }
        frac = new Fraction(1, 1);
        if(!"1".equals(frac.toString())) {
            System.err.println("toString with a denominator of 1 seems broken");
        }
        frac = new Fraction(1, -1);
        if(!"-1".equals(frac.toString())) {
            System.err.println("toString with a denominator of 1 seems broken");
        }
        Fraction addend1 = new Fraction(1, 2);
        Fraction addend2 = new Fraction(1, -2);
        if(!addend1.plus(addend2).equals(new Fraction(0))) {
            System.err.println("It appears plus, equals, or 1 arg constructor is broken for " + addend1 + " + " + addend2
            + " = " + addend1.plus(addend2));
        }

    }

    private int num;
    private int den;


    public Fraction(int num) {
        this(num, 1);
    }

    public Fraction() {
        this((int)(Math.random()*21)-10,(int)(Math.random()*10)+1);
    }

    public Fraction(int num, int den) {
        this.num = num;
        if(den<0) {
            this.den = -den;
            this.num *= -1; // this.num = this.num * -1;
        } else if(den == 0) {
            this.den = 1;
            System.err.println("Invalid denominator setting it to 1");
        } else {
            this.den = den;
        }
        reduce();
    }

    public Fraction(String answer) {
        int index = answer.indexOf('/');
        String numerator = answer.substring(0, index);
        String denominator = answer.substring(index+1);
        Fraction temp = new Fraction(Integer.parseInt(numerator), Integer.parseInt(denominator));
        num = temp.num;
        den = temp.den;
    }

    @Override
    public boolean equals(Object that) {
        Fraction frac = (Fraction)that;
        return this.num*frac.den==frac.num*this.den;
    }

    @Override
    public String toString() {
        String result = "" + num;
        if(den!=1) {
            result += "/" + den;
        }
        return result;
    }

    private void reduce() {
        // TODO
        // make num/den in reduced form
    }

    /**
     * Calculates the sum of two fractions.
     * @param that the fraction to be added
     * @return The sum of the two fractions
     */
    public Fraction plus(Fraction that) {
        return new Fraction(this.num*that.den + that.num*this.den, this.den*that.den);
    }
}

















/*    public Fraction plus(Fraction that) {
        return new Fraction(this.num*that.den + that.num*this.den, this.den*that.den);
    }

    public Fraction(String answer) {
        int index = answer.indexOf('/');
        if(index==-1) {
            num = Integer.parseInt(answer);
            den = 1;
        } else {
            num = Integer.parseInt(answer.substring(0, index));
            den = Integer.parseInt(answer.substring(index+1));
        }
    }

    public boolean equals(Fraction that) {
        return num==that.num && den==that.den;
    }
*/
