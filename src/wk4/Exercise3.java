package wk4;

import javax.swing.*;

public class Exercise3 {
    public static void main(String[] args) {
        String phrase = JOptionPane.showInputDialog(null, "Enter a phrase");
        for(int i=0; i<phrase.length(); ++i) {
            char character = phrase.charAt(i);
            if(Character.isDigit(character)) {
                System.out.println(character + " is a digit");
            } else if(Character.isLetter(character)) {
                String message = "letter";
                if(character=='a' || character=='e' || character=='i' || character=='o' || character=='u'
                   || character=='A' || character=='E' || character=='I' || character=='O' || character=='U') {
                    message = "vowel (which is a letter)";
                }
                System.out.println(character + " is a " + message);
            }
        }
    }
}
