package wk2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        /* 17 is big, 3 is small
           write program that will do some calculation to produce
           5 as result
        */
        Scanner  in = new java.util.Scanner(System.in);
        System.out.print("enter a big number and a small number (separated by spaces): ");
        int big = in.nextInt();
        int small = in.nextInt();
        System.out.println(-(big * small));
    }
}
