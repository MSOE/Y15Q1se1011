package wk1;/*
Steps for running a program
1. Write program in .java file
2. Compile program
   The process of converting Java statements into Java Byte Code (.class)
3. Execute program
   Loading program into Java Virtual Machine (JVM) and executing
 */
/*
 * Course: SE-1011
 * Term: Fall 2014
 * Assignment: Lab 1
 * Author: Dr. Chris Taylor
 * Date: 09/14/09
 * Modified: 08/09/14 - updated for Fall 2014
 */

import java.util.Scanner;

/**
 * Class containing the entire program for lab 1 in fall 2014
 *  SE-1011 course.
 * @author taylor
 * @version 2014-08-09-1.4
 */
public class Lab1 {

    /**
     * Simple program to calculate the amount of money a user
     * will earn in one year.
     * @param args ignored
     */
    public static void main(String[] args) {
        // Create a "reference variable"/object to gather data
        // from the keyboard
        Scanner in = new Scanner(System.in);

        final int weeksInYear = 52;

        // Request data from the user
        System.out.print("Enter the number of hours worked per week: ");
 //       long hoursWorked = in.nextInt();
        long hoursWorked = 5;
        System.out.print("Enter the number of weeks of vacation taken: ");
        int vacationWeeks = in.nextInt();
        System.out.print("Enter your hourly wage (in dollars): ");
        float hourlyWage = (float)in.nextDouble();

        boolean useless = true;

        System.out.print("Enter \"your\" first initial: ");
        String initial = in.next();
        char letter = '\"';

        // Calculate earnings
        double yearlyEarnings = hoursWorked * (weeksInYear - vacationWeeks)
                * hourlyWage;

        System.out.println("You will earn $" + yearlyEarnings
                + " in one year.");
    }

}