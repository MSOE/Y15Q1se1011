package wk10;

import javax.swing.*;
import java.util.ArrayList;

public class Driver {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        String word;
        do {
            word = JOptionPane.showInputDialog(null, "Enter a word, please");
            if(word!=null) {
                words.add(word);
            }
        } while(word!=null);

        for(int i=0; i<words.size(); ++i) {
            words.set(i, words.get(i).toUpperCase());
        }

        words.remove("THE");

        if(words.contains("THE")) {
            System.out.println("I found The");
        }

        // For each loop
        // For each string wrd in words, do the following
        for(String wrd : words) {
            System.out.println(wrd);
        }


        /*
        for(int i=0; i<words.size(); ++i) {
            String wrd = words.get(i);
            System.out.println(wrd);
         */
    }
}
