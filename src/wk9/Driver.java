package wk9;

import java.util.Scanner;

/*
int[] data;              // Declaring an array variable
data = new int[SIZE];    // Create an array of SIZE integers and have data point to it
int size = data.length;  // Get the length of the array
data[INDEX] = 3;         // Assign 3 to the (INDEX+1)th element of the array
int value = data[INDEX]; // Assign value the (INDEX+1)th element from the array
int[] data2 = {3, 2, 1, 0};     // Creates an array with 4 element
 */
public class Driver {
    public static void main(String[] args) {
        double[] data = {Math.PI, Math.E, 8.0, 3.1};
        data = addToArray(data, 6.6);
        System.out.println(data.length);
    }

    private static double[] addToArray(double[] data, double v) {
        double[] newbie = new double[data.length+1];
        for(int i=0; i<data.length; ++i) {
            newbie[i] = data[i];
        }
        newbie[newbie.length-1] = v;
        return newbie;
    }


    public static void main3(String[] args) {
        char[][] board = new char[3][3];
        for(int row=0; row<board.length; ++row) {
            for(int col=0; col<board[0].length; ++col) {
                board[row][col] = ' ';
            }
        }
        board[0][0] = 'X';
        board[1][1] = 'O';
        board[2][0] = 'X';
        board[1][0] = 'O';
        printBoard(board);
    }

    /*
         |   |
      -----------
         |   |
      -----------
         |   |
     */
    private static void printBoard(char[][] board) {
        printRow(board[0]);
        System.out.println("-----------");
        printRow(board[1]);
        System.out.println("-----------");
        printRow(board[2]);
    }

    private static void printRow(char[] chars) {
        System.out.print(' ');
        System.out.print(chars[0]);
        System.out.print(" | ");
        System.out.print(chars[1]);
        System.out.print(" | ");
        System.out.print(chars[2]);
        System.out.println(' ');
    }


    public static void main2(String[] args) {
        // Get number of quizzes
        System.out.println("Please enter the number of quizzes you've had.");
        Scanner in = new Scanner(System.in);
        int numberOfQuizzes = in.nextInt();

        int[] scores = getQuizScores(in, numberOfQuizzes);

        int lowestScore = findLowestQuizScore(scores);

        int totalScores = getTotalScores(scores);

        double quizGrade = calculateGrade(numberOfQuizzes, lowestScore, totalScores);
        System.out.println("Your quiz grade average is: " + quizGrade*10);
    }

    private static double calculateGrade(int numberOfQuizzes, int lowestScore, int totalScores) {
        totalScores -= lowestScore;
        return (double)totalScores/(numberOfQuizzes-1);
    }

    private static int getTotalScores(int[] scores) {
        int totalScores = 0;
        for(int i=0; i<scores.length; ++i) {
            totalScores += scores[i];
        }
        return totalScores;
    }

    private static int findLowestQuizScore(int[] scores) {
        int lowestScore = scores[0];
        for(int i=1; i<scores.length; ++i) {
            lowestScore = Math.min(lowestScore, scores[i]);
        }
        return lowestScore;
    }

    private static int[] getQuizScores(Scanner in, int numberOfQuizzes) {
        int[] scores = new int[numberOfQuizzes];
        for(int i=0; i<numberOfQuizzes; ++i) {
            System.out.println("Enter your score for quiz " + (i + 1));
            scores[i] = in.nextInt();
        }
        return scores;
    }
}
