package wk4;

import javax.swing.*;
import java.util.Scanner;

public class Exercise4 {
    public static void main(String[] args) {
        String phrase = JOptionPane.showInputDialog(null, "Enter a phrase");
        Scanner in = new Scanner(phrase);
        while(in.hasNext()) {
            System.out.println(in.next());
        }

    }

    public static void main2(String[] args) {
        System.out.println("Enter a phrase");
        Scanner in = new Scanner(System.in);
        String phrase = in.nextLine();
        int start = 0;
        for(int end = 0; end<phrase.length(); ++end) {
            if(phrase.charAt(end)==' ') {
                System.out.println(phrase.substring(start, end));
                start = end+1;
            }
        }
        System.out.println(phrase.substring(start));
    }
}
