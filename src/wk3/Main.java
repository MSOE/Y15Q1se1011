package wk3;

import java.util.Scanner;

/*

    if ( CONDITIONAL ) {
        STATEMENTS that run if CONDITIONAL is TRUE
    } else {
        STATEMENTS that run if CONDITIONAL is FALSE
    }

 */
public class Main {
    public static void main(String[] args) {
        int i;
        boolean a = true;
        boolean b = false;
        boolean c = (!a && b) || (a && !b);
        System.out.println("Enter an integer");
        Scanner in = new Scanner(System.in);
        i = in.nextInt();
        boolean wasFive = false;
        if (i<5) { // less than 5
            System.out.println(i + " is less than 5");
        } else if(i==5) {
            System.out.println(i + " is equal to 5");
            wasFive = true;
            i = 10;
        }  else  {
            System.out.println(i + " is greater than 5");
        }
        System.out.println(wasFive + " now " + i);

    }
}
