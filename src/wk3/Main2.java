package wk3;

import javax.swing.*;
import java.util.Scanner;

public class Main2 {
    /*
      switch ( VARIABLE ) {
      case MATCH1:
          STATEMTENTS to run if VARIABLE==MATCH1
          break;
      case MATCH2:
          STATEMTENTS to run if VARIABLE==MATCH2
          break;
      default:
          STATEMENTS to run if did match on any of the above
      }
     */
    public static void main(String[] args) {

        String name = JOptionPane.showInputDialog("Enter your name");
        switch (Character.toUpperCase(name.charAt(0))) {
            case 'A':
                JOptionPane.showMessageDialog(null, "You begin with an A, well, at least your name does.");
                break;
            case 'E':
                JOptionPane.showMessageDialog(null, "You begin with an E, well, at least your name does.");
                break;
            case 'I':
                JOptionPane.showMessageDialog(null, "You begin with an I, well, at least your name does.");
                break;
            case 'O':
                JOptionPane.showMessageDialog(null, "You begin with an O, well, at least your name does.");
                break;
            case 'U':
                JOptionPane.showMessageDialog(null, "You begin with an U, well, at least your name does.");
                break;
            default:
                JOptionPane.showMessageDialog(null, "You begin with an consonant, well, at least your name does.");
        }

    }



























    public static void main2(String[] args) {
        String answer = JOptionPane.showInputDialog("Enter your score");
//        int score = Integer.parseInt(answer);
        double score = Double.parseDouble(answer);
        String grade = "F";
        if(score>=93) {
            grade = "A";            // Input: 90
        } else if(score>=89) {      //  Line  in      score    grade   Output
            grade = "AB";           //   7                             Enter your score
        } else if(score>=84) {      //   8   Sys.in
            grade = "B";            //   9   Sys.in     90
        } else if(score>=80) {      //  10   Sys.in     90      "F"
            grade = "BC";           //  11   Sys.in     90      "F"
        } else if(score>=75) {      //  13   Sys.in     90      "F"
            grade = "C";            //  14   Sys.in     90      "AB"
        } else if(score>=70) {      //  26   Sys.in     90      "AB"   Your grade was: AB
            grade = "CD";
        } else if(score>=65) {
            grade = "D";
        }
        JOptionPane.showMessageDialog(null, "Your grade was: " + grade);
    }
}
