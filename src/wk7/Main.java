package wk7;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main4(String[] args) {
        System.out.println("Welcome to the fraction quiz");
        Scanner in = new Scanner(System.in);
        boolean done = false;
        while(!done) {
            Fraction addend1 = new Fraction();
            Fraction addend2 = new Fraction();
            System.out.println(addend1 + " + " + addend2);
            String answer = in.next();
            if(answer.equals("q")) {
                done = true;
            } else {
                Fraction sumHuman = new Fraction(answer); // "1/2"
                Fraction sum = addend1.plus(addend2);
                if(sum.equals(sumHuman)) {
                    System.out.println("Correct");
                } else {
                    System.out.println("Incorrect, the correct answer is: " + addend1.plus(addend2));
                }
            }

//            System.out.println(answer);
        }
    }

    public static void main2(String[] args) {
        Fraction[] fractions;
        fractions = new Fraction[50];
        for(int i=0; i<fractions.length; i++) {
            fractions[i] = new Fraction((int)(Math.random()*50));
        }
        for(int j=0; j<fractions.length; j++) {
            System.out.println(fractions[j]);
        }
    }

    public static void main(String[] args) {
        ArrayList<Fraction> fractions;
        fractions = new ArrayList<>();
        for(int i=0; i<50; i++) {
            fractions.add(new Fraction((int)(Math.random()*50), (int)(Math.random()*20)+1));
        }
        for(Fraction f : fractions) {
            System.out.println(f);
        }
        List<Fraction> fracs = fractions.subList(10, 20);
        System.out.println("Sublist");
        for(Fraction f : fracs) {
            System.out.println(f);
        }

    }
}



















