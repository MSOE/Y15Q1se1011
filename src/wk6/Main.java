package wk6;

public class Main {
    public static void main(String[] args) {
        Complex josh = new Complex();
        System.out.println(josh);
        Complex comp = new Complex(2.0, -3.0);
        Complex comp2 = new Complex(2.0, -3.0);
        Complex comp3 = new Complex(5.0, -12.0);
        Complex comp4 = comp.multipliedBy(comp2);
//        Complex.togglePolarForm();
        System.out.println(comp4);
        System.out.println(comp3);
        System.out.println(comp3.equals(comp4));
    }
}
