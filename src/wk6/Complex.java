/*
 * Course: SE1011-011
 * Term: Fall 2014
 * Assignment: Lectures
 * Author: Chris Taylor
 * Date: 10/21/14
 */
package wk6;

/**
 * Complex class that represent a complex number with real and imaginary components.
 *
 * <pre>
 * +------------------------------------------+
 * |                 Complex                  |
 * +------------------------------------------+
 * | -real: double                            |
 * | -imag: double                            |
 * +------------------------------------------+
 * | +Complex()                               |
 * | +Complex(real: double)                   | HW
 * | +Complex(real: double, imag: double)     |
 * | +plus(comp: Complex): Complex            |
 * | +minus(comp: Complex): Complex           | HW
 * | +multipliedBy(comp: Complex): Complex    |
 * | +magnitude(): double                     |
 * | +toString(): String                      | HW (finish)
 * | +equals(comp: Complex): boolean          |
 * +------------------------------------------+
 * </pre>
 * http://taylor.kilnhg.com/se1011
 * @author taylor and class
 * @version 2014.10.08a
 *
 */
public class Complex {
    /** Display object in polar form if true, otherwise display in
     * normally.
     */
    private static boolean displayPolar = false;

    /** The real component of the complex number */
    private double real;

    /** The imaginary component of the complex number */
    private double imag;

    /**
     * Create a complex object with the value 0.0 + i0.0
     */
    public Complex() {
        this(0.0, 0.0);
    }

    /**
     * Create a complex object with specified value
     * @param real The real component of the complex number
     * @param imag The imaginary component of the complex number
     */
    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public static void togglePolarForm() {
        displayPolar = !displayPolar;
    }

    /**
     * Returns a string representation of the object.
     * e.g., 0.0 + i3.2
     *       0.0 - i3.2
     *       2.0
     *       -2.0 + i4.6
     *       New HW: deal with 0.0 + i3.2  and 0.0 - i3.2 to not show the 0.0
     * @return The string representation
     */
    public String toString() {
        String result = null;
        if(displayPolar) {
            result = magnitude() + " /_ " + angle();
        } else {
            result = "" + real;
            if (imag > 0) {
                result += " + i" + imag;
            } else if (imag < 0) {
                result += " - i" + -imag;
            }
        }
        return result;
    }

    /**
     * Return the result of adding the calling object and comp.
     * @param comp The number to be added
     * @return The sum of the two complex numbers
     */
    public Complex plus(Complex comp) {
        // Complex a = new Complex(w, x);
        // Complex b = new Complex(y, z);
        // a + b == (w + y) + i(x + z)
        return new Complex(real+comp.real, imag+comp.imag);
    }

    /**
     * Return the result of multiplying the calling object with comp.
     * @param comp The number to multiply the call object by.
     * @return The product of the the call object and comp
     */
    public Complex multipliedBy(Complex comp) {
        // Complex a = new Complex(w, x);
        // Complex b = new Complex(y, z);
        // a * b == (w + ix) * ((y + iz) == (w*y - x*z) + i(xy + wz)
        return new Complex(real*comp.real - imag*comp.imag, imag*comp.real + real*comp.imag);

    }

    /**
     * Return true if two objects contain the same value.
     * @param comp Object to compare with
     * @return true if both objects are the same
     */
    public boolean equals(Complex comp) {
        boolean equals = this==comp;
        if(!equals) {
            equals = real == comp.real && imag == comp.imag;
        }
        return equals;
    }

    /**
     * Swap the value of two complex objects.
     *
     * The calling object takes on the value of the object passed to the method and
     * the object passed in takes on the value of the calling object.
     * @param comp The object to swap values with
     */
    public void swap(Complex comp) {
        double temp = real;
        real = comp.real;
        comp.real = temp;
        temp = imag;
        imag = comp.imag;
        comp.imag = temp;
    }

    /**
     * Calculates the magnitude of the complex number
     * @return The magnitude of the complex number
     */
    public double magnitude() {
        return Math.sqrt(real*real + imag*imag);
    }

    public double angle() {
        return Math.toDegrees(Math.atan(imag/real));
    }
}

