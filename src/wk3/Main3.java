package wk3;

import javax.swing.*;
import java.util.Scanner;

/*
  while( CONDITIONAL ) {
      STATEMENTS that run if CONDITIONAL is true
  } // GO BACK AND EVALUATE CONDITIONAL AGAIN

  do {
       STATEMENTS to run at least once (possibly more)
  } while( CONDITIONAL );

  for( INITIALIZATION; CONDITIONAL; UPDATE) {
       STATEMENTS to run if CONDITIONAL true
  }
  1. INITIALIZATION
  2. CONDITIONAL
  3. STATEMENTS (if CONDITIONAL was true)
  4. UPDATE
  5. CONDITIONAL
  6. STATEMENTS (if CONDITIONAL was true)
  7. UPDATE
  8. CONDITIONAL
 */
public class Main3 {
    public static void main(String[] args) {
        int i=10;
        for( ; i>0; i=i-1) {
            System.out.println(i);
        }
    }
}





